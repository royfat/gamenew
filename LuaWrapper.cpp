/* 
 * File:   LuaWrapper.cpp
 * Author: filip
 * 
 * Created on den 29 juli 2015, 20:02
 */

#include "LuaWrapper.h"
#include "ShaderUtils.h"
#include "SpriteHandler.h"

LuaWrapper::LuaWrapper() {
}

LuaWrapper::LuaWrapper(const LuaWrapper& orig) {
}

LuaWrapper::~LuaWrapper() {
}

void LuaWrapper::resetLuaState(){

    TextureUtils::resetTextures();
    std::cout << "LUA OPENLIBS" << std::endl;
    this->L = NULL;
    this->L = luaL_newstate();
    luaL_openlibs(L);
    
    std::string path = ShaderUtils::getexepath();
    
    Luna<LuaSpriteHandler>::Register(L);
    #ifdef __APPLE__
    path= path.substr(0, path.find_last_of("\\/"));
    path = "/Users/filip/gamenew/Assets/";
    #endif
	#ifdef _WIN32
		path = "C:\\Users\\thebo\\Desktop\\Game\\Assets\\";
	#endif


    lua_pushstring(L, path.c_str());
    lua_setglobal(L, "PATH");
#ifdef __APPLE__
	path += "run.lua";
#elif _WIN32
	path += "run.lua";
#endif
    int errorloadfile =   luaL_loadfile(L, (path).c_str());
    std::cout << "path:" << path << std::endl;
    std::cout << "LUA SCRIPT LOADED: " <<  errorloadfile << std::endl;
    int error = lua_pcall(L, 0, 0, 0);
    if(error == LUA_ERRRUN){
        std::cout  <<"LUA_ERRRUN"  << std::endl;
    }
    if(error == LUA_ERRMEM){
        std::cout  <<"LUA_ERRMEM"  << std::endl;
    }
    if(error == LUA_ERRERR){
        std::cout  <<"LUA_ERRERR"  << std::endl;
    }
    
    std::cout << "LUA CALLED WITH EXITED WITH CODE:  " << error << std::endl;

}
struct timeval gametime;
void LuaWrapper::ondraw(){
	 lua_getglobal(L, "onDraw");
     gettimeofday(&gametime,NULL);
    lua_pushinteger(L,floor(gametime.tv_usec/1000));
    lua_setglobal(L, "TIME");
	 lua_pcall(L, 0, 0, 0);

}

void LuaWrapper::onanimation() {
	lua_getglobal(L, "onAnimation");
	lua_pcall(L, 0, 0, 0);

}

void LuaWrapper::onMouseEvent(int x, int y){
	lua_getglobal(L, "onMouseEvent");
	lua_pushnumber(L, x);
	lua_pushnumber(L, y);

	lua_pcall(L, 2, 0, 0);
}

void LuaWrapper::oninit(){
	 lua_getglobal(L, "onInit");
    
	 int error =  lua_pcall(L, 0, 0, 0);

}
void LuaWrapper::onquit(){
	 lua_getglobal(L, "onQuit");
	 int error =  lua_pcall(L, 0, 0, 0);

}

void LuaWrapper::onevent(SDL_Keycode k){
	if (k == SDLK_TAB) {
		this->resetLuaState();
	}
	lua_getglobal(L, "onInput");
        lua_pushnumber(L, k);
	int error =  lua_pcall(L, 1, 0, 0);
}

