//
//  TextureUtils.cpp
//  Game
//
//  Created by filip on 2015-07-26.
//


#include "TextureUtils.h"
//
//  TextureHandler.cpp
//  mysdl
//
//  Created by filip on 2014-12-23.
//  Copyright (c) 2014 filip. All rights reserved.
//

GLuint i =0;
GLuint textures[4] = {0,0,0,0};

GLuint TextureUtils::generateTexture(unsigned const  char *data,int width, int height){
    
    
    GLuint texture = 0;
   
    glActiveTexture(GL_TEXTURE0+i);
    glGenTextures(1, &texture );
    glBindTexture(GL_TEXTURE_2D, texture);
    
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,data);
    textures[i] = texture;
    i++;
    
    return texture;
    
    
    
}

void TextureUtils::resetTextures() {
 
    
    glDeleteTextures(i,textures);

    textures[0] =0;
    
    textures[1] =0;
    textures[2] =0;
    textures[3] =0;
    i = 0;

    
    
}




GLuint TextureUtils::loadTexture(char const* filename){
    
    std::vector< unsigned char>  image;
    unsigned width,height;
    
    lodepng::decode(image,width,height,filename);
    
    
    return generateTexture(&image.data()[0],width,height);
}


std::vector< unsigned char> TextureUtils::readpng(char const* filename){
    
    std::vector< unsigned char>  image;
    unsigned width,height;
    lodepng::decode(image,width,height,filename);
    
    return image;
}



float TextureUtils::LookupMap(float x, float y,std::vector< unsigned char> img){
    
   // 1024, 768,
  //  x = 512 - (512*x);
  //  y = 381 - (381*y);
    
    
    x  =  160 - (160*x);
    y  =  100 - (100*y);
    int pos = ((320*(int)y)+(int)x);
    if(pos*4 > img.size())
        return 0.0;
    if(pos < 0.0)
        return 0.0;
    
    unsigned int r = img[pos * 4 + 2];
    unsigned int g = img[pos * 4 + 1];
    unsigned int b = img[pos * 4 + 0];
    
    float delta =  1- (1) ;
    float color = ((float)r/255);
    float  a =  color*delta;
    float out =1+(a);
    
    return r;
    
}




std::vector<char> TextureUtils::ReadAllBytes(char const* filename)
{
    
    std::ifstream ifs (filename,std::ifstream::binary|std::ifstream::in);
    ifs.seekg(0,ifs.end);
    std::ifstream::pos_type pos = ifs.tellg();
    ifs.seekg(0,ifs.end);
    
    std::vector<char>  result(pos);
    
    ifs.seekg(0, std::ios::beg);
    ifs.read(&result[0], pos);
    
    //fi.read(reinterpret_cast<char*>(temp), );
    return result;
}


void TextureUtils::encodeBMP(std::vector<unsigned char>& bmp, const unsigned char* image, int w, int h)
{
    //3 bytes per pixel used for both input and output.
    int inputChannels = 3;
    int outputChannels = 3;
    
    //bytes 0-13
    bmp.push_back('B'); bmp.push_back('M'); //0: bfType
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); //2: bfSize; size not yet known for now, filled in later.
    bmp.push_back(0); bmp.push_back(0); //6: bfReserved1
    bmp.push_back(0); bmp.push_back(0); //8: bfReserved2
    bmp.push_back(54 % 256); bmp.push_back(54 / 256); bmp.push_back(0); bmp.push_back(0); //10: bfOffBits (54 header bytes)
    
    //bytes 14-53
    bmp.push_back(40); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //14: biSize
    bmp.push_back(w % 256); bmp.push_back(w / 256); bmp.push_back(0); bmp.push_back(0); //18: biWidth
    bmp.push_back(h % 256); bmp.push_back(h / 256); bmp.push_back(0); bmp.push_back(0); //22: biHeight
    bmp.push_back(1); bmp.push_back(0); //26: biPlanes
    bmp.push_back(outputChannels * 8); bmp.push_back(0); //28: biBitCount
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //30: biCompression
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //34: biSizeImage
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //38: biXPelsPerMeter
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //42: biYPelsPerMeter
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //46: biClrUsed
    bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //50: biClrImportant
    
    /*
     Convert the input RGBRGBRGB pixel buffer to the BMP pixel buffer format. There are 3 differences with the input buffer:
     -BMP stores the rows inversed, from bottom to top
     -BMP stores the color channels in BGR instead of RGB order
     -BMP requires each row to have a multiple of 4 bytes, so sometimes padding bytes are added between rows
     */
    
    int imagerowbytes = outputChannels * w;
    imagerowbytes = imagerowbytes % 4 == 0 ? imagerowbytes : imagerowbytes + (4 - imagerowbytes % 4); //must be multiple of 4
    
    for(int y = h - 1; y >= 0; y--) //the rows are stored inversed in bmp
    {
        int c = 0;
        for(int x = 0; x < imagerowbytes; x++)
        {
            if(x < w * outputChannels)
            {
                int inc = c;
                //Convert RGB(A) into BGR(A)
                if(c == 0) inc = 2;
                else if(c == 2) inc = 0;
                bmp.push_back(image[inputChannels * (w * y + x / outputChannels) + inc]);
            }
            else bmp.push_back(0);
            c++;
            if(c >= outputChannels) c = 0;
        }
    }
    
    // Fill in the size
    bmp[2] = bmp.size() % 256;
    bmp[3] = (bmp.size() / 256) % 256;
    bmp[4] = (bmp.size() / 65536) % 256;
    bmp[5] = bmp.size() / 16777216;
}
