#ifdef _WIN32
#ifndef  opengl3h
#define opengl3h
#pragma once
#include <stdio.h>
#include "SDL.h"

#include "SDL_opengl.h"

//SDL GL CONTEXT NEED TO BE SET 
//typedef void (APIENTRY * GL_ActiveTextureARB_Func)(unsigned int);
typedef GLuint (APIENTRY * GL_CreateProgram_Func)();
typedef void (APIENTRY * GL_genBufers_Func)(GLsizei n, GLuint *buffer);
typedef void (APIENTRY* GL_glGenVertexArrays_FUNC)(GLsizei n, GLuint *arrays);
typedef void (APIENTRY * GL_BindVertexArray_func)(GLuint array);
typedef void (APIENTRY* GL_BindBuffer_func)(GLenum target,GLuint buffer);
typedef void (APIENTRY * GL_Bufferdata_func)(GLenum  target,GLsizeiptr size, const GLvoid *data, GLenum usage);
typedef void (APIENTRY * GL_VertexAttributePointer_func)(GLuint index, GLint  	size,GLenum type, GLboolean normalized,	GLsizei stride,const GLvoid *pointer);
typedef GLuint (APIENTRY *GL_GetError_func)();
typedef GLuint (APIENTRY *GL_CreateShader_func)(GLenum shaderType);
typedef GLuint (APIENTRY *GL_ShaderSource_func)(GLuint shader,GLsizei count,const GLchar **string,const GLint *length);
typedef void (APIENTRY *GL_CompileShader_func)(GLuint shader);
typedef void (APIENTRY* GL_AttachShader_func)(GLuint program, GLuint shader);
typedef void (APIENTRY * GL_BindAttributeLocation_func)(GLuint program,GLuint index,const GLchar *name);
typedef void (APIENTRY *GL_LinkProgram)(GLuint program);
typedef void (APIENTRY *GL_ValidateProgram)(GLuint program);
typedef void (APIENTRY *GL_UseProgram)(GLuint program);
typedef void (APIENTRY *GL_ActiveTexture_func)(GLenum texture);
typedef void (APIENTRY *GL_BindTexture_func)(GLenum target,GLuint texture);
typedef void (APIENTRY * GL_EnableVertexAttribArray)(GLuint index);
typedef void (APIENTRY * GL_DisableVertexAttributeArray)(GLuint index);
typedef GLint (APIENTRY * GL_getUniformLocation)(GLuint program,const GLchar *name);
typedef void (APIENTRY * GL_glGetShaderiv_func)(	GLuint shader,GLenum pname,GLint *params);
typedef void (APIENTRY *GL_glGetProgramiv_func)( GLuint program,GLenum pname, GLint *params);
typedef void (APIENTRY *GL_glGetPrograminfoLog)( GLuint program,GLsizei maxLength,GLsizei *length,GLchar *infoLog);
typedef void (APIENTRY *GL_glGetShaderInfoLog_func)(GLuint shader,GLsizei maxLength,GLsizei *length,GLchar *infoLog);
typedef void (APIENTRY *GL_glglUniformMatrix4fv_func) (	GLint location,GLsizei count,GLboolean transpose,const GLfloat *value);
typedef void (APIENTRY *GL_glUniform3f_func)(GLint location,GLfloat v0,GLfloat v1,GLfloat v2);
typedef void (APIENTRY *glUniform4f_func)(GLint location,GLfloat v0,GLfloat v1,GLfloat v2,GLfloat v3);

typedef void (APIENTRY *glUniform1i_func)(GLint location,GLint v0);
typedef void (APIENTRY *glDetachShader_func)(GLuint program,GLuint shader);

typedef void (APIENTRY *gldeleteshader_func)(GLuint shader);


typedef GLint(APIENTRY *glGetAttributeLocation_func)(GLuint  	program,const GLchar * 	name);




void static glUniform1i(	GLint location,GLint v0){

	glUniform1i_func glUniform1i_ptr  =  (glUniform1i_func)SDL_GL_GetProcAddress("glUniform1i");
	glUniform1i_ptr(location,v0);
}

/*
typedef void (APIENTRY *glGenTextures_func)(GLsizei n, GLuint * textures);
typedef void (APIENTRY *glBindTexture_func)(GLenum target,GLuint texture);
typedef void (APIENTRY *glTexParameterf_func)(GLenum target,GLenum pname,GLfloat param);

typedef void (APIENTRY * glTexImage2D_func)(	GLenum target,GLint level,GLint internalformat,GLsizei width,GLsizei height,GLint border,GLenum format,GLenum type,const GLvoid * data);

void glTexImage2D(	GLenum target,GLint level,GLint internalformat,GLsizei width,GLsizei height,GLint border,GLenum format,GLenum type,const GLvoid * data){

	glTexImage2D_func glTexImage2D_ptr = 0;
	glTexImage2D_ptr =  (glTexImage2D_func) SDL_GL_GetProcAddress("glTexImage2D");
	glTexImage2D_ptr(target,level,internalformat,width,height,border,format,type,data);
}

void glTexParameterf(	GLenum target,GLenum pname,GLfloat param){

	glTexParameterf_func glTexParameter_ptr = 0;
	glTexParameter_ptr =  (glTexParameterf_func) SDL_GL_GetProcAddress("glTexParameterf");
	glTexParameter_ptr(target,pname,param);
}



void glBindTexture(	GLenum target,GLuint texture){

	glBindTexture_func glBindTexture_ptr = 0;
	
	glBindTexture_ptr =  (glBindTexture_func) SDL_GL_GetProcAddress("glBindTexture");
	glBindTexture_ptr(target,texture);
}

void glGenTextures(	GLsizei n,GLuint * textures) {

	glGenTextures_func glGenTextures_ptr = 0;
	glGenTextures_ptr =  (glGenTextures_func) SDL_GL_GetProcAddress("glGenTextures");
	glGenTextures_ptr(n,textures);

}
*/

GLint static  glGetAttribLocation(GLuint  	program, const GLchar * 	name) {

	glGetAttributeLocation_func glGetAttributeLocation_func_ptr = 0;
	glGetAttributeLocation_func_ptr = (glGetAttributeLocation_func)SDL_GL_GetProcAddress("glGetAttribLocation");
	return glGetAttributeLocation_func_ptr(program, name);
}
void static glActiveTexturea(GLenum texture) {
	GL_ActiveTexture_func activitytexture_ptr = 0;
	activitytexture_ptr = (GL_ActiveTexture_func)SDL_GL_GetProcAddress("glActiveTexture");
	activitytexture_ptr(texture);

}


void static glDetachShader(GLuint program,GLuint shader){
 

	glDetachShader_func glDetachShader_func_ptr = 0;
	glDetachShader_func_ptr =  (glDetachShader_func) SDL_GL_GetProcAddress("glDetachShader");
	glDetachShader_func_ptr(program,shader);

}

 
void static glUniform4f(	GLint location,GLfloat v0,GLfloat v1,GLfloat v2,GLfloat v3){

	glUniform4f_func GL_gluniform4_ptr = 0;
	GL_gluniform4_ptr =  (glUniform4f_func) SDL_GL_GetProcAddress("glUniform4f");
	GL_gluniform4_ptr(location,v0,v1,v2,v3);

}


void static glUniform3f(	GLint location,GLfloat v0,GLfloat v1,GLfloat v2) {

	GL_glUniform3f_func GL_glUniform3f_ptr = 0;
	
	GL_glUniform3f_ptr = (GL_glUniform3f_func)SDL_GL_GetProcAddress("glUniform3f");
	GL_glUniform3f_ptr(location,v0,v1,v2);
}
 

void static glUniformMatrix4fv(	GLint location,GLsizei count,GLboolean transpose,const GLfloat *value){
	GL_glglUniformMatrix4fv_func GL_glglUniformMatrix4fv_ptr = 0;
	GL_glglUniformMatrix4fv_ptr = (GL_glglUniformMatrix4fv_func)SDL_GL_GetProcAddress("glUniformMatrix4fv");

	GL_glglUniformMatrix4fv_ptr(location,count,transpose,value);
}



void static glGetShaderInfoLog(	GLuint shader,GLsizei maxLength,GLsizei *length,GLchar *infoLog){
	GL_glGetShaderInfoLog_func glGetShaderinfolog_ptr = 0;
	glGetShaderinfolog_ptr = (GL_glGetShaderInfoLog_func) SDL_GL_GetProcAddress("glGetShaderInfoLog");
	glGetShaderinfolog_ptr(shader,maxLength,length,infoLog);

}

  void static glGetProgramInfoLog(	GLuint program,GLsizei maxLength,GLsizei *length,GLchar *infoLog){

	GL_glGetPrograminfoLog GL_glGetPrograminfoLog_ptr = 0;
	GL_glGetPrograminfoLog_ptr = (GL_glGetPrograminfoLog) SDL_GL_GetProcAddress("glGetProgramInfoLog");
	GL_glGetPrograminfoLog_ptr(program,maxLength,length,infoLog);

}


void static glGetProgramiv(	GLuint program,GLenum pname,GLint *params){

	GL_glGetProgramiv_func gl_glgetprogram = 0;
	gl_glgetprogram = (GL_glGetProgramiv_func) SDL_GL_GetProcAddress("glGetProgramiv");
	gl_glgetprogram(program,pname,params);
}
 


void static glGetShaderiv(	GLuint shader,GLenum pname,GLint *params){
	GL_glGetShaderiv_func gl_glGetShad = 0;
	gl_glGetShad = (GL_glGetShaderiv_func) SDL_GL_GetProcAddress("glGetShaderiv");
	gl_glGetShad(shader,pname,params);

}


GLint static glGetUniformLocation(	GLuint program, const GLchar *name){

	GL_getUniformLocation uniform_ptr =0;
	uniform_ptr = (GL_getUniformLocation) SDL_GL_GetProcAddress("glGetUniformLocation");
	return uniform_ptr(program,name);

}


void static glDisableVertexAttribArray(GLuint index){

	GL_DisableVertexAttributeArray disable_ptr =0;
	disable_ptr = (GL_DisableVertexAttributeArray) SDL_GL_GetProcAddress("glDisableVertexAttribArray");
	disable_ptr(index);
}

 
void static glEnableVertexAttribArray(GLuint index){

	GL_EnableVertexAttribArray enableVerxt_ptr = 0;
	enableVerxt_ptr = (GL_EnableVertexAttribArray) SDL_GL_GetProcAddress("glEnableVertexAttribArray");
	enableVerxt_ptr(index);
}

/*
void glBindTexture(GLenum target, GLuint texture){

	GL_BindTexture_func bindTexture_ptr = 0;
	bindTexture_ptr = (GL_BindTexture_func) SDL_GL_GetProcAddress("glBindTexture");
	bindTexture_ptr(target,texture);
}
*/




void static glUseProgram(GLuint program){
	GL_UseProgram useprogram_ptr = 0;
	useprogram_ptr = (GL_UseProgram) SDL_GL_GetProcAddress("glUseProgram");
	useprogram_ptr(program);

}

void static glValidateProgram(GLuint program){

	GL_ValidateProgram validateprogram_ptr = 0;
	validateprogram_ptr = (GL_ValidateProgram) SDL_GL_GetProcAddress("glValidateProgram");
	validateprogram_ptr(program);
	
}

void static glLinkProgram(GLuint program){
	
	GL_LinkProgram  linkprogram_ptr = 0;
	linkprogram_ptr = (GL_LinkProgram) SDL_GL_GetProcAddress("glLinkProgram");
	linkprogram_ptr(program);

}

void static glBindAttribLocation(GLuint program, GLuint index, const GLchar *name){

	GL_BindAttributeLocation_func bindattri_ptr = 0;
	bindattri_ptr = (GL_BindAttributeLocation_func) SDL_GL_GetProcAddress("glBindAttribLocation");
	bindattri_ptr(program,index,name);
}

void static glAttachShader(GLuint program, GLuint shader){

	GL_AttachShader_func attach_ptr = 0;
	attach_ptr = (GL_AttachShader_func) SDL_GL_GetProcAddress("glAttachShader");
	attach_ptr(program,shader);
}

void static glCompileShader(GLuint shader){
	GL_CompileShader_func compileshader_ptr =  0;
	compileshader_ptr = (GL_CompileShader_func) SDL_GL_GetProcAddress("glCompileShader");
	compileshader_ptr(shader);
}


void static glShaderSource(	GLuint shader,	GLsizei count,const GLchar **string,const GLint *length) {

	GL_ShaderSource_func glcreateShaderSource_ptr = 0;
	glcreateShaderSource_ptr = (GL_ShaderSource_func) SDL_GL_GetProcAddress("glShaderSource");
	glcreateShaderSource_ptr(shader,count,string,length);

}

GLuint static glCreateShader(GLenum shadertype){

	GL_CreateShader_func glcreateshader_ptr = 0;
	glcreateshader_ptr = (GL_CreateShader_func) SDL_GL_GetProcAddress("glCreateShader");
	return glcreateshader_ptr(shadertype);

}
/*
GLuint glGetError(){

	GL_GetError_func geterror_ptr = 0;
	geterror_ptr = (GL_GetError_func) SDL_GL_GetProcAddress("glGetError");
	GLuint errorValue = geterror_ptr();
	return errorValue;

}
*/


void static glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer) {

	GL_VertexAttributePointer_func GL_VertexAttributePointer_ptr = 0;
	GL_VertexAttributePointer_ptr = (GL_VertexAttributePointer_func) SDL_GL_GetProcAddress("glVertexAttribPointer");
	GL_VertexAttributePointer_ptr(index,size,type,normalized,stride,pointer);

}



GLuint static glCreateProgram(){

	GL_CreateProgram_Func  glCreateProgram_ptr = 0;
	glCreateProgram_ptr = (GL_CreateProgram_Func) SDL_GL_GetProcAddress("glCreateProgram");
	return glCreateProgram_ptr();

}

void  static glGenBuffers(GLsizei n, GLuint *buffers){

	GL_genBufers_Func glgenBufers_ptr = 0;
	glgenBufers_ptr = (GL_genBufers_Func) SDL_GL_GetProcAddress("glGenBuffers");
	 glgenBufers_ptr(n,buffers);
	
} 

void static glGenVertexArrays(	GLsizei n,	   GLuint *arrays){

	GL_glGenVertexArrays_FUNC glgenvert_ptr = 0;
	glgenvert_ptr = (GL_glGenVertexArrays_FUNC) SDL_GL_GetProcAddress("glGenVertexArrays");
	return glgenvert_ptr(n,arrays);
}

 void static glBindVertexArray(GLuint array){

	GL_BindVertexArray_func bindvertex_ptr = 0;
	bindvertex_ptr = (GL_BindVertexArray_func) SDL_GL_GetProcAddress("glBindVertexArray");
	bindvertex_ptr(array); 

}
  void static   glBindBuffer(	GLenum target, GLuint buffer){

	GL_BindBuffer_func bindbuffer_ptr = 0;
	bindbuffer_ptr = (GL_BindBuffer_func) SDL_GL_GetProcAddress("glBindBuffer");
	bindbuffer_ptr(target,buffer);


}

 void static glBufferData(GLenum  target,GLsizeiptr size, const GLvoid *data, GLenum usage){

	GL_Bufferdata_func GL_Bufferdata_ptr = 0;
	GL_Bufferdata_ptr = (GL_Bufferdata_func) SDL_GL_GetProcAddress("glBufferData");
	GL_Bufferdata_ptr(target,size,data,usage);
}


 
void static   glDeleteShader(GLuint shader) {
 

	gldeleteshader_func gldeleteshader_func_ptr = 0;
	gldeleteshader_func_ptr =  (gldeleteshader_func) SDL_GL_GetProcAddress("glDeleteShader");
	gldeleteshader_func_ptr(shader);


 }

#endif
#endif