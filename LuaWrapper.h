/* 
 * File:   LuaWrapper.h
 * Author: filip
 *
 * Created on den 29 juli 2015, 20:02
 */

#include "luna.h"
#include "SDL.h"
#include <sys/time.h>
#ifndef LUAWRAPPER_H
#define	LUAWRAPPER_H


class LuaWrapper {
public:
    LuaWrapper();
    lua_State *L ;
    void ondraw();
    void oninit();
    void onquit();
	void onanimation();
    void onevent(SDL_Keycode k);
    void resetLuaState();
	void onMouseEvent(int x, int y);
    LuaWrapper(const LuaWrapper& orig);
    virtual ~LuaWrapper();
private:

};

#endif	/* LUAWRAPPER_H */

