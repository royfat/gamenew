//
//  Shaderprogram.h
//  Game
//
//  Created by filip on 2015-07-26.
//

#ifndef __Game__Shaderprogram__
#define __Game__Shaderprogram__

#include <vector>
#include <map>
#include "ShaderUtils.h"

#include "opengl3.c"
#include <fstream>


class Shaderprogram {
    
public:
    std::string  *fragmentShaderUrl;
    std::string  *vertexShaderUrl;
    std::map<std::string,GLint>  &uniformsMatrix;
    std::map<std::string,GLint> &uniformsVec4names;
    GLuint vertexId;
    GLuint fragmentId;
    GLuint program;
    GLuint textureid;
    GLint *textures;
    void HandleOwnUniforms();
    Shaderprogram(std::string &fragmentURl,std::string &vertexUrl,std::vector<std::string> uniformsNames,std::vector<std::string>uniformsVec4vector);
    
    ~Shaderprogram();
};


#endif /* defined(__Game__Shaderprogram__) */
