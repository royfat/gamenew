//
//  ShaderUtils.h
//  Game
//
//  Created by filip on 2015-07-26.
//

#ifndef __Game__ShaderUtils__
#define __Game__ShaderUtils__

#include <iostream>
#ifdef __APPLE__

#include <unistd.h>
#include <OpenGL/gl3.h>

#include <libproc.h>

#include <errno.h>
#endif
#include <string.h>
#include <fstream>
#include "opengl3.c"

class ShaderUtils{
public:
    
    static GLuint loadShader(int shadertype,std::string  shadertext);
    static std::string loadFile(const char *file);
    static  GLuint GenerateProgram(GLuint vertexId,GLuint fragmentId);
    
    static  std::string getexepath();
    static void printProgramInfoLog(GLuint obj);
    
    static void printShaderInfoLog(GLuint obj);
    static void checkGlError(std::string msg);
    
};
#endif /* defined(__Game__ShaderUtils__) */
