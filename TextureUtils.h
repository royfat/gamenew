//
//  TextureUtils.h
//  Game
//
//  Created by filip on 2015-07-26.
//

#ifndef __Game__TextureUtils__
#define __Game__TextureUtils__



#ifdef __APPLE__
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <OpenGL/gl3.h>
#include "OpenGL/glu.h"
#elif _WIN32
#include <direct.h>

#include "SDL_opengl.h"
#define GetCurrentDir _getcwd
#endif
#include <fstream>
#include <vector>
#include "lodepng.h"




struct Tileset {
    
    GLuint textureid;
	GLfloat width;
	GLfloat height;
    GLfloat tileWidth;
	GLfloat tileHeight;
    const char *name;
    GLuint firstgid;
    GLuint spaccing;
    GLuint margin;
    const char* image;
	GLfloat imagewidth;
	GLfloat imageheight;
    float tileUVw;
    float tileUVh;
    

};

struct Layer {
	const char * type;
	const char *	name;
	GLfloat	x;
	GLfloat  y;
	GLuint	width;
	GLuint	height;
	bool	visible;
	float	opacity;
	std::vector<int> data;
	int tilesetposition;
    std::vector<int> *spritepos;

};


class TextureUtils{
    
private:
    std::vector<char> *vec;
    
public:
    static GLuint generateTexture(unsigned const char *data,int width,int height);
    static std::vector< unsigned char> readpng(char const* filename);
    static std::vector<char> ReadAllBytes(char const* filename);
    static void resetTextures();
    static GLuint loadTexture(char const* filename);
    static float LookupMap(float x, float y,std::vector< unsigned char> img);
    
    static void encodeBMP(std::vector<unsigned char>& bmp, const unsigned char* image, int w, int h);
    static std::string getAPPlicationDirectory(){
#ifdef _WIN32
        wchar_t the_path[MAX_PATH];
        
        GetModuleFileName(NULL, the_path, MAX_PATH) ;
        
#ifdef _DEBUG
        return std::string("C:\\Users\\fille\\Documents\\Visual Studio 2012\\Projects\\MysecondSDL\\MysecondSDL\\boxit\\mysdl\\");
#endif
        return std::string((char*) the_path);
        
#elif __APPLE__
        char the_path[256];
        getcwd(the_path, 255);
        strcat(the_path, "/");
#ifdef DEBUG
        return std::string("/Users/filip/gamenew/");
#endif
        return std::string(the_path);
        
#endif
        
    }
    
    
    
    
};



#endif /* defined(__Game__TextureUtils__) */
