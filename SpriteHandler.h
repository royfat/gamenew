 //
//  Sprite.h
//  Game
//
//  Created by filip on 2015-07-26.
//

#ifndef __Game__Sprite__
#define __Game__Sprite__

#include <stdio.h>
#include "ShaderUtils.h"

#include "glm.hpp"
#include "matrix_transform.hpp"
#include "TextureUtils.h"
#include "luna.h"      
#include "lua.h"
#include <stdio.h>
#include <cmath>
struct Sprite{
    
    GLuint  textureid;
    glm::vec3 position;  //3  12
    glm::vec3 rotation;  //3  12
    bool update;                    //1  1
    glm::mat4 modelMatrix;   //16 64
    glm::vec2 atlaspos;     // padding 4
	glm::vec2 tileSize;
	glm::vec2 scale;
    int type;
					           //    92
    
};


class SpriteHandler {
    
    
public:
    SpriteHandler();
    void draw();
    GLuint fragmentid;
    GLuint vertexid;
    GLuint textureCoords;
    GLuint program;
    GLuint vposition;
    GLuint mmatrix;
    GLuint textures;
    GLuint cameraid;
	GLuint tilesizeid;
    
    GLuint textureid;
    GLuint atlaspos;
	GLuint scaleid;
    glm::mat4 *viewmatrix;
    glm::mat4 *projection;
    void updateSpriateMatrix(int index);
    void LoadSprites();
    glm::vec3 *camerapos;
    std::vector<Sprite> *sp;
    std::vector<Tileset> *tiles;
	std::vector<Layer> *layers;
    std::vector<unsigned char> imgte;

};

class LuaSpriteHandler {
public:
    	 static  const char className[];
	 static Luna<LuaSpriteHandler>::RegType methods[];
         
          LuaSpriteHandler(lua_State *L){
              
                realobject = new SpriteHandler();
          }
          
         int setCamera(lua_State *L){
             
             realobject->camerapos->x = luaL_checknumber(L,1);
             realobject->camerapos->y = luaL_checknumber(L,2);
             realobject->camerapos->z = luaL_checknumber(L,3);
            return 1; 
         }
         int draw(lua_State *L){
             realobject->draw();
             return 1;
         }
         
         int AddAtlas(lua_State *L){
			 unsigned int size;
			 Tileset text;
             lua_getfield(L,-1,"firstgid");
             text.firstgid = luaL_checkinteger(L,-1);
             lua_getfield(L,-2,"tilewidth");
             text.tileWidth = luaL_checkinteger(L,-1);
             lua_getfield(L,-3,"tileheight");
             text.tileHeight = luaL_checkinteger(L,-1);
             lua_getfield(L,-4,"spacing");
             text.spaccing =  luaL_checkinteger(L,-1);
             lua_getfield(L,-5,"margin");
             text.margin = luaL_checkinteger(L,-1);
             lua_getfield(L,-6,"image");
			 text.image = luaL_checkstring(L, -1);
             lua_getfield(L,-7,"imagewidth");
             text.imagewidth = luaL_checkinteger(L,-1);
             lua_getfield(L,-8,"imageheight");
             text.imageheight = luaL_checkinteger(L,-1);
             
             text.textureid =  TextureUtils::loadTexture(text.image);
             
             text.tileUVw = text.tileWidth/(text.imagewidth-text.margin);
             text.tileUVh = text.tileHeight/(text.imageheight-text.margin);
              
             realobject->tiles->push_back(text); 
             return 1;
            
            
         }

		 int addSprite(lua_State *L) {
			 Sprite spr;
			 lua_getfield(L, -1,"textureid");
			 spr.textureid = luaL_checkinteger(L, -1);
			 lua_getfield(L, -2 , "x");
             spr.position.x = luaL_checknumber(L, -1);
			
			 lua_getfield(L, -3, "y");
			 spr.position.y= luaL_checknumber(L, -1);
			 lua_getfield(L, -4,"atlasX");
			 spr.atlaspos.x = luaL_checknumber(L, -1);
			 lua_getfield(L, -5,"atlasY");
			 spr.atlaspos.y =  luaL_checknumber(L, -1);

			 lua_getfield(L, -6, "tileW");
			 spr.tileSize.x = luaL_checknumber(L, -1);
			 lua_getfield(L, -7, "tileH");
			 spr.tileSize.y = luaL_checknumber(L, -1);

			 lua_getfield(L, -8, "scaleX");
			 spr.scale.x = luaL_checknumber(L, -1);
			 lua_getfield(L, -9, "scaleY");
			 spr.scale.y = luaL_checknumber(L, -1);
             spr.update = true;
			 realobject->sp->push_back(spr);
			 lua_pushinteger(L, realobject->sp->size()-1); 
			/* lua_createtable(L, 0, 3);
			 lua_pushnumber(L, pos.x);
			 lua_setfield(L, -2, "x");
			 lua_pushnumber(L, pos.y);
			 lua_setfield(L, -2, "y");
			 lua_pushnumber(L, pos.z);
			 lua_setfield(L, -2, "z");
			 */
			 return 1;
		 }
    int getSpritePos(lua_State *L) {
        int index = luaL_checkinteger(L, -1);
        Sprite sp = realobject->sp->at(index);
        lua_newtable(L);
        
        lua_pushnumber(L, sp.position.x);
        lua_rawseti(L, -2, 1);
        lua_pushnumber(L, sp.position.y);
        lua_rawseti(L, -2, 2);
     
        return 1;
        
    }
    
    int checkHeightMap(lua_State *L){
        
        
        
         int index = luaL_checkinteger(L, 1);
       
        Sprite sp = realobject->sp->at(index);
        
        float AmaxX =  (sp.position.x +0.05);
        float AminX =  ( sp.position.x -0.05 );
        
        
        float AmaxY =   (sp.position.y +0.05 );
        float AminY =   ( sp.position.y -0.05) ;
        
        
        int hit = 0;
        int side = 0;
        int vert = 0;
        for (int i = 0;i < realobject->layers->size();i++){
            Layer cur =  realobject->layers->at(i);
            int siz = cur.spritepos->size();
            for(int j = 0;j < siz ;j++){
               
                int curspra = cur.spritepos->at(j);
                Sprite sprit =   realobject->sp->at(curspra);
                if (sprit.type == 3)
                    continue;
                
                
                
                float BmaxX =  ( sprit.position.x + 0.05);
                float BminX =  ( sprit.position.x - 0.05);
                
                
                float BmaxY =   (sprit.position.y + 0.05);
                float BminY =  ( sprit.position.y - 0.05);
                
             //   if(sprit.position.y !=  sp.position.y)
               //     continue;
               
                if(AmaxX >= BminX && AminX <= BmaxX && AmaxY >= BminY && AminY <= BmaxY){
                    
                    if((AminY+0.01) >= BmaxY ){
                    
                        
                        hit = 1;
                        vert = 2;
                        
                    }
                    
                    if((AmaxY+0.01) <= BminY ){
                        
                        
                        hit = 0;
                        vert = 2;
                        
                    }
                    
                    
                    
                    if((AmaxX+0.05) >= BmaxX ){
                        
                        
                        
                        hit = 1;
                        side = 0;
                        break;
                    }else
                    
                    if((AminX +0.05) <= BminX ){
                        
                        hit = 1;
                        side = 1;
                        
                        break;
                    }
                    
                    
                    
                }
                
            }
            
        }
  //      std::cout << k << std::endl;
        lua_newtable(L);
        lua_pushinteger(L, hit);
        lua_rawseti(L, -2, 1);
        lua_pushinteger(L, side);
        lua_rawseti(L, -2, 2);
        lua_pushinteger(L, vert);
        lua_rawseti(L, -2, 3);
        return 1;
        
        
    }
 
		 int setSpritePosition(lua_State *L) {
			 int index = luaL_checkinteger(L, 1);
			 Sprite sp = realobject->sp->at(index);

			 sp.position.x  = luaL_checknumber(L, 2);
			 sp.position.y = luaL_checknumber(L, 3);
			 sp.position.z = luaL_checknumber(L, 4);
			 sp.update = true;
			 realobject->sp->at(index) = sp;
			 return 1;
		 }
		 int setSpriteAtlas(lua_State *L) {
			 int index = luaL_checkinteger(L, 1);
			 Sprite sp = realobject->sp->at(index);

			 sp.atlaspos.x = luaL_checknumber(L, 2);
			 sp.atlaspos.y = luaL_checknumber(L, 3);
			 sp.update = true;
			 realobject->sp->at(index) = sp;
			 return 1;
		 }

		 int AddLayer(lua_State *L) {

			 unsigned int size;
			 Layer lay;
			 lua_getfield(L, -1, "name");
			 lay.name= (char*)luaL_checkstring(L, -1);
			 lua_getfield(L, -2, "type");
			 lay.type = (char*)luaL_checkstring(L, -1);
			 
			 lua_getfield(L, -3, "x");
			 lay.x = luaL_checknumber(L, -1);
			 lua_getfield(L, -4, "y");
			 lay.y = luaL_checknumber(L, -1);
			 lua_getfield(L, -5, "width");
			 lay.width = luaL_checkinteger(L, -1);
			 lua_getfield(L, -6, "height");
			 lay.height = luaL_checkinteger(L, -1);
			 lua_getfield(L, -7, "visible");
			 lay.visible = (bool)lua_toboolean(L, -1);
			 lua_getfield(L, -8, "opacity");
			 lay.opacity = luaL_checkinteger(L, -1);
			 lua_getfield(L, -9, "data");
			 std::vector<int> v;
			 lua_pushnil(L);
			 int index = lua_gettop(L);
			 while (lua_next(L, -2)) {
				 v.push_back(luaL_checkinteger(L, -1));
				 lua_pop(L, 1);
			 }

			 lay.tilesetposition = 0;
			 lay.data = v;
             lay.spritepos = new std::vector<int>();
			 realobject->layers->push_back(lay);


			 
			 return 1;
		 }
    
    int setLayerPosition(lua_State *L){
        
        
        
        
        
        for (int i = 0; i < realobject->layers->size();i++) {
            
            Layer tempLayer =  realobject->layers->at(i);
            
            for (int j = 0; j < tempLayer.spritepos->size();j++)
            {
                
                Sprite sp = realobject->sp->at(tempLayer.spritepos->at(j));
                sp.position.x  =  sp.position.x + luaL_checknumber(L, 1);
                sp.position.y =  sp.position.y +  luaL_checknumber(L, 2);
                sp.position.z =  sp.position.z +  luaL_checknumber(L, 3);
                sp.update = true;
                realobject->sp->at(tempLayer.spritepos->at(j)) = sp;
                
            }
            
        }
        
        
        return 0;
    }
    
    
		 int CreateSprites(lua_State *L) {
             for (int i = 0; i < realobject->layers->size();i++) {

				Layer tempLayer =  realobject->layers->at(i);
                tempLayer.spritepos = new std::vector<int>();
				Tileset set = realobject->tiles->at(tempLayer.tilesetposition);
				

				 for (int j = 0; j < tempLayer.data.size(); j++) {
					 Sprite tempSprite;
					 float a = (float) tempLayer.data[j];
                     float atlasY = floor((float) a / tempLayer.width);
                     float atlasX = (  (int)a) % tempLayer.width;

					 float posY = floor(j / tempLayer.width);
					 float posX = abs(j - (posY * tempLayer.width));
                     tempSprite.textureid = 1;//set.textureid;
					 tempSprite.position = glm::vec3(posX*0.1, posY*-0.1,0.0);
					 tempSprite.position.x += tempLayer.x;
					 tempSprite.position.y += tempLayer.y;
					 tempSprite.scale.x += 1.0;
					 tempSprite.scale.y += 1.0;
                     tempSprite.type = a;
					 tempSprite.atlaspos = glm::vec2(atlasX, atlasY);
					 tempSprite.tileSize = glm::vec2(set.tileUVw, set.tileUVh);
					 
                     tempLayer.spritepos->push_back(realobject->sp->size());
					 realobject->sp->push_back(tempSprite);
                     realobject->layers->at(i) =tempLayer;
					 tempSprite.update = true;
				 }
			 }

			 return 1;
		 }
		
          
	 SpriteHandler *realobject;
    
};
#endif /* defined(__Game__Sprite__) */

/*
  name = "map",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "tiles.png",
      imagewidth = 128,
      imageheight = 128,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
   
 * 
 *    tiles = {}
   
 * 
 * {
      name = "map",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "tiles.png",
      imagewidth = 128,
      imageheight = 128
 * } 
 */
