//
//  main.cpp
//  Game
//


#include <iostream>
#include "SDL.h"
#undef main
#include "Shaderprogram.h"
#include "SpriteHandler.h"
#include "ctime"
#include "LuaWrapper.h"
#define GL3_PROTOTYPES 1


static Uint32 next_time;
SDL_Window *window;
int x,y;
void run();
int EventHandler();
LuaWrapper *wrap;
bool running = true;
Uint32 FPS = 60;





int main() {
    

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    unsigned int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    window = SDL_CreateWindow("GAME", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 1024, 768, flags);
    SDL_GLContext  glc = SDL_GL_CreateContext(window);
    if (!glc){
        fprintf(stderr, "Could not create the OpenGL 3.2 context for the window: %s\n",
        SDL_GetError());
        return false;
    }else {
    
    run();
    }
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

void run() {
    
    
    glEnable(GL_MULTISAMPLE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear (GL_COLOR_BUFFER_BIT);
    glViewport(0,0,1024,768);
    wrap = new LuaWrapper();
    wrap->resetLuaState();
   Uint32 start = SDL_GetTicks();
   Uint32 curretTime = SDL_GetTicks();
   Uint32 lastTime = SDL_GetTicks();
   Uint32 dTime = 0;
   Uint32 anispeed = 0;
   while(running){

	   curretTime = SDL_GetTicks();
	   if (curretTime > lastTime) {
		   dTime = curretTime - lastTime;
		   lastTime = curretTime;
		   anispeed += dTime;
	   }
	   if (anispeed > 67) {
		   wrap->onanimation();
		   anispeed = 0;
	   }

       EventHandler();
       glClear (GL_COLOR_BUFFER_BIT);
       wrap->ondraw();
       SDL_GL_SwapWindow(window);
       if(1000/FPS > SDL_GetTicks()-start){
           SDL_Delay(1000/FPS -(SDL_GetTicks()-start));
       }
      
       
    }
   
}
    
    


int   EventHandler() {
     SDL_Event  test_event;
     while (SDL_PollEvent(&test_event)) {
         switch(test_event.type){
             case SDL_WINDOWEVENT:{
                  switch(test_event.window.event)
                  {
                      case SDL_WINDOWEVENT_CLOSE:{
                       running =false;
                       break;
                      }
                  }
             }
             case SDL_KEYDOWN:{
				 
                 wrap->onevent(test_event.key.keysym.sym);
                 break;
             }
			 case SDL_MOUSEMOTION: {
				 wrap->onMouseEvent(test_event.motion.x, test_event.motion.y);
				 break;

			 }
		
         }
                 
         // handle your event here
     }
    
    return 1;
}
