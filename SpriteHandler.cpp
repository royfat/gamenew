//
//  Sprite.cpp
//  Game
//
//  Created by filip on 2015-07-26.
//


#include "SpriteHandler.h"





static unsigned int drawOrder[] = { 0, 1, 2, 0, 2, 3 };

static  float coords[] = {
    -0.5f,  0.5f, 0.0f,   // top left
    -0.5f, -0.5f, 0.0f,   // bottom left
    0.5f, -0.5f, 0.0f,   // bottom right
    0.5f,  0.5f, 0.0f }; // top right

static float texture[] = {
    
     0.000100,1- 0.999900,
     0.000100,1- 0.000100,
     0.999900,1- 0.000100,
     0.999900,1- 0.999900
    
};



SpriteHandler::SpriteHandler(){
    
    
    
    camerapos = new glm::vec3(0,0,0);
    

	#ifdef __APPLE__
	std::string s1 = ShaderUtils::getexepath();

    
    //std::reverse(imgte.begin(),imgte.end());
    
	s1 = s1.substr(0, s1.find_last_of("\\/"));
    std::string vertex =   ShaderUtils::loadFile(std::string(s1+"/vertex.glsl").c_str());
    std::string fragment =   ShaderUtils::loadFile(std::string(s1 + "/fragment.glsl").c_str());
	#elif _WIN32
		std::string s1 = ShaderUtils::getexepath();
		std::string vertex = ShaderUtils::loadFile(std::string(s1+ "\\Assets\\vertex.glsl").c_str());
		std::string fragment = ShaderUtils::loadFile(std::string(s1 +"\\Assets\\gridfrag.glsl").c_str());
	#endif
    float ratio = (float) 1024 / 780;
    fragmentid =   ShaderUtils::loadShader(GL_FRAGMENT_SHADER, fragment);
    vertexid =  ShaderUtils::loadShader(GL_VERTEX_SHADER, vertex);
    program =  ShaderUtils::GenerateProgram(vertexid, fragmentid);
    
    vposition =  glGetAttribLocation(program, "vPosition");
	textureCoords =  glGetAttribLocation(program, "coords");
    mmatrix = glGetUniformLocation(program,"uMVPMatrix");
    textureid = glGetUniformLocation(program,"textureid");
    cameraid = glGetUniformLocation(program,"camera");
    atlaspos = glGetUniformLocation(program,"atlaspos");
	tilesizeid = glGetUniformLocation(program, "tilesize");
	scaleid = glGetUniformLocation(program, "scale");
	layers = new std::vector<Layer>();
    tiles = new std::vector<Tileset>();
	sp = new std::vector<Sprite>();
    
}

void SpriteHandler::updateSpriateMatrix(int index){
     Sprite cur =  sp->at(index); 
    
    if(cur.update){
    cur.modelMatrix = glm::mat4(1);
    cur.modelMatrix = glm::translate(cur.modelMatrix, cur.position);
    cur.modelMatrix = glm::rotate(cur.modelMatrix, cur.rotation.x, glm::vec3(1,0,0));
    cur.modelMatrix = glm::rotate(cur.modelMatrix, cur.rotation.y, glm::vec3(0,1,0));
    cur.modelMatrix = glm::rotate(cur.modelMatrix, cur.rotation.z, glm::vec3(0,0,1));
	cur.modelMatrix = glm::scale<float>(cur.modelMatrix, glm::vec3(0.1));
    cur.update = false;
    sp->at(index) = cur;
    
    }
    
}
void SpriteHandler::draw(){
    
    glUseProgram(program);;
    glEnableVertexAttribArray(vposition);
    glEnableVertexAttribArray(textureCoords);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glActiveTexture(GL_TEXTURE1);
   
    glm::mat4 uvp(1);
    glVertexAttribPointer(vposition, 3, GL_FLOAT, false, 3*4, &(coords[0]));
    glVertexAttribPointer(textureCoords, 2, GL_FLOAT, false, 2*4, &(texture[0]));
    
    for(int i = 0;i < sp->size();i++){
		Sprite currentSP = sp->at(i);
		glBindTexture(GL_TEXTURE_2D, currentSP.textureid);
      
        updateSpriateMatrix(i);
        glUniformMatrix4fv(mmatrix,1,GL_FALSE,&(sp->at(i).modelMatrix[0][0]));
        glUniform3f(cameraid, camerapos->x, camerapos->y, camerapos->z);
		
        glUniform1i(textureid, 1);
        glUniform3f(atlaspos, currentSP.atlaspos.x, currentSP.atlaspos.y ,0.0);
		glUniform3f(scaleid, currentSP.scale.x, currentSP.scale.y, 0.0);
		glUniform3f(tilesizeid, currentSP.tileSize.x, currentSP.tileSize.y,0.0);
        glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT, &(drawOrder[0]));
    }
    glDisableVertexAttribArray(vposition);
    glDisableVertexAttribArray(textureCoords);
    glUseProgram(0);
    
    
}

void SpriteHandler::LoadSprites(){
    
	#ifdef __APPLE__

		std::string s1 = ShaderUtils::getexepath();
		s1 = s1.substr(0, s1.find_last_of("\\/"));
		std::string asssetURL("/Users/filip/gamenew/Assets/sprites.bin");
	#elif _WIN32
		std::string s1 = ShaderUtils::getexepath();
		std::string asssetURL(s1 + "\\Assets\\sprites.bin");
	#endif
    std::ifstream fi (std::string( asssetURL).c_str(),std::ifstream::binary|std::ifstream::in);
    
    sp = new std::vector<Sprite>();
    unsigned char tempVert[sizeof(Sprite)];
        for(int i = 0;i < 1;i++){
        fi.read(reinterpret_cast<char*>(tempVert), sizeof(Sprite));
        Sprite a = reinterpret_cast<Sprite&>(tempVert);
        a.update = true;
        sp->push_back(a);
         
    }
    
    
}

#define method(class, name) {#name, &class::name}

const char LuaSpriteHandler::className[] = "LuaSpriteHandler";


Luna<LuaSpriteHandler>::RegType LuaSpriteHandler::methods[] = {
	method(LuaSpriteHandler,setCamera),
	method(LuaSpriteHandler,draw),
	method(LuaSpriteHandler,AddAtlas),
	method(LuaSpriteHandler,AddLayer),
	method(LuaSpriteHandler,CreateSprites),
	method(LuaSpriteHandler,addSprite),
	method(LuaSpriteHandler,setSpritePosition),
	method(LuaSpriteHandler,setSpriteAtlas),
    method(LuaSpriteHandler,getSpritePos),
    method(LuaSpriteHandler,checkHeightMap),
    method(LuaSpriteHandler,setLayerPosition),
    {0,0}
    
};
