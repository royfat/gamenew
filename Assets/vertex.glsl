
attribute vec3 vPosition;
attribute  vec2 coords;
uniform mat4 uMVPMatrix;
uniform vec3 camera;
uniform vec3 scale;
varying vec2 v_TexCoordinate;

void main() {

  v_TexCoordinate = coords;
  gl_Position = uMVPMatrix*vec4(vPosition+camera,1);
  gl_Position.x*= scale.x;
  gl_Position.y*= scale.y;
	
  }