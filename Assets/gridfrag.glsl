
uniform vec3 atlaspos;
uniform vec3 tilesize;
varying vec2 v_TexCoordinate;
uniform sampler2D textureid;
void main() {

float  x =((atlaspos.x+0.014)*tilesize.x)+v_TexCoordinate.x*(tilesize.x-0.0002);
float  y =  (atlaspos.y*tilesize.y)+v_TexCoordinate.y*tilesize.y;

  gl_FragColor = texture2D(textureid,vec2(x,y));

}