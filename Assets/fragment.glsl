
uniform vec3 atlaspos;
uniform vec3 tilesize;
varying vec2 v_TexCoordinate;
uniform sampler2D textureid;
void main() {

float  x =((atlaspos.x+0.014)*tilesize.x)+v_TexCoordinate.x*(tilesize.x-0.0002);
float  y =  (atlaspos.y*tilesize.y)+v_TexCoordinate.y*tilesize.y;

 	 
 vec4 coords = gl_FragCoord;
 float a =  mod(coords.x,16.0);

 float b =  mod(coords.y,16.0);

 
	gl_FragColor = texture2D(textureid,vec2(x,y));
 if(floor(a) == 0.0) {
 //gl_FragColor = vec4(0,0,0,1);
 }
 
 if(floor(b) == 0.0) {
 //gl_FragColor = vec4(0,0,0,1);
 } 
 

}