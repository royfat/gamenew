//
//  Shaderprogram.cpp
//  Game
//
//  Created by filip on 2015-07-26.
//

#include "Shaderprogram.h"


 

Shaderprogram::Shaderprogram(std::string &fragmentURl,std::string &vertexUrl,std::vector<std::string> uniformsNames,std::vector<std::string> uniformsVec4vector):fragmentShaderUrl(new std::string(fragmentURl)),vertexShaderUrl(new std::string(vertexUrl)),uniformsMatrix(*new std::map<std::string,GLint>()),uniformsVec4names(*new std::map<std::string,GLint>()){
    
    for(std::string uni : uniformsNames){
        
        uniformsMatrix[uni] = -1;
    }
    for(std::string uni1 : uniformsVec4vector){
        
        uniformsVec4names[uni1] = -1;
    }
    
    textures = new GLint[3];
    textures[0] = -1;
    textures[1] = -1;
    textures[3] = -1;
    std::string  	vertexShader = ShaderUtils::loadFile(vertexUrl.c_str());
    std::string  fragmentShader = ShaderUtils::loadFile(fragmentURl.c_str());
    vertexId =  ShaderUtils::loadShader(GL_VERTEX_SHADER, vertexShader);
    fragmentId = ShaderUtils::loadShader(GL_FRAGMENT_SHADER, fragmentShader);
    
    program = glCreateProgram();
    glAttachShader(program,vertexId);
    glAttachShader( program,fragmentId);
    
    
    glBindAttribLocation( program, 0, "vPosition");
    glBindAttribLocation( program, 1, "in_TextureCoord");
    glBindAttribLocation( program, 2, "in_normals");
    glBindAttribLocation( program, 3, "in_group");
    
    glLinkProgram(program);
    glValidateProgram( program);
    glDetachShader(program,vertexId);
    glDetachShader(program,fragmentId);
    glDeleteShader(vertexId);
    glDeleteShader(fragmentId);
    GLint stat;
    glGetProgramiv( program, GL_LINK_STATUS, &stat);
    ShaderUtils::checkGlError("GL_LINK_STATUS");
    for(auto  kvp : uniformsMatrix) {
        
        uniformsMatrix[kvp.first]   =  glGetUniformLocation( program,kvp.first.c_str());
        ShaderUtils::checkGlError("fel");
    }
    
    for(auto  kvp : uniformsVec4names) {
        
        //if the uniforms is not used in the shader  the glGetUniformLocation will return -1 on some cards.
        //there samplers etc will return -1 when not used.
        uniformsVec4names[kvp.first]   =  glGetUniformLocation( program,kvp.first.c_str());
        
    }
    
    
}

void Shaderprogram::HandleOwnUniforms() {
    
    /*glm::vec3 pos = *cam.getPosition();
    
    int text = GL_TEXTURE0;
    int uniform = 1;
    if(uniformsVec4names["normalmapTexture"] > -1) {
        glActiveTexture(text+uniform);
        glBindTexture(GL_TEXTURE_2D,textures[0]);
        glUniform1i((GLint)uniformsVec4names["normalmapTexture"],uniform);
        uniform++;
    }
    if(uniformsVec4names["renderedTexture2"] > -1) {
        glActiveTexture(GL_TEXTURE0+uniform);
        glBindTexture(GL_TEXTURE_2D,textures[1]);
        glUniform1i((GLint)uniformsVec4names["renderedTexture2"],uniform);
        uniform++;
    }
    
    if(textures[2] < 268835921) {
        glActiveTexture(GL_TEXTURE0+uniform);
        glBindTexture(GL_TEXTURE_2D,textures[2]);
        glUniform1i((GLint)uniformsVec4names["shadowmap"],uniform);
        uniform++;
        
    }
    */
    
}

Shaderprogram::~Shaderprogram() {
    
}


/*
const char LuaShaderprogram::className[] = "LuaShaderprogram";


Luna<LuaShaderprogram>::RegType LuaShaderprogram::methods[] = {
    method(LuaShaderprogram,setTexture),
    {0,0}
    
};


const char LuaShaderprogram::className[] = "LuaShaderprogram";

*/