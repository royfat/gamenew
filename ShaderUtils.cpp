//
//  ShaderUtils.cpp
//  Game
//
//  Created by filip on 2015-07-26.
//

#include "ShaderUtils.h"


GLuint ShaderUtils::loadShader(int shadertype,std::string  shadertext){
    
    GLuint aid = glCreateShader(shadertype);
    const GLchar *source =shadertext.c_str();
    const GLint len = shadertext.length();
    glShaderSource(aid, 1, &source, &len);
    glCompileShader(aid);
    printShaderInfoLog(aid);
    return aid;
    
}

std::string ShaderUtils::loadFile(const char *file){
    
    std::ifstream shaderfile(file);
    
    std::string str((std::istreambuf_iterator<char>(shaderfile)),
                    std::istreambuf_iterator<char>());
    
    
    return str;
}


GLuint ShaderUtils::GenerateProgram(GLuint vertexId,GLuint fragmentId){
    
    
    GLuint program = glCreateProgram();
    glAttachShader(program,vertexId);
    glAttachShader(program,fragmentId);
    
    
    glLinkProgram(program);
    glValidateProgram(program);
    printProgramInfoLog(program);
    return program;
}


void ShaderUtils::printProgramInfoLog(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
    
    glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
    
    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
    }
}

void ShaderUtils::printShaderInfoLog(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
    
    glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
    
    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
    }
}

void ShaderUtils::checkGlError(std::string msg){
    
    GLuint errorValue = glGetError();
    
    if (errorValue != GL_NO_ERROR) {
    std::cout << msg.c_str() << ":" << errorValue << std::endl;
    }
}




 std::string ShaderUtils::getexepath()
{
#ifdef __APPLE__
    int ret;
    pid_t pid; 
    char pathbuf[PROC_PIDPATHINFO_MAXSIZE];

    pid = getpid();
    ret = proc_pidpath (pid, pathbuf, sizeof(pathbuf));
    return "/Users/filip/gamenew/Assets/";
    if ( ret <= 0 ) {
        return std::string(pathbuf);
    } else {
        return std::string(pathbuf);
    }
    

#elif _WIN32
	return std::string("C:\\Users\\thebo\\Desktop\\Game\\");
#endif
}
    

    
