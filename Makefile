#OBJS specifies which files to compile as part of the project
OBJS = main.cpp SpriteHandler.cpp ShaderUtils.cpp TextureUtils.cpp lodepng.cpp LuaWrapper.cpp
LIBPATH =  -L/usr/local/lib/  
LIBPATH +=  -L"libs/lua"
LIBPATH +=  -L"libs/glm"
LIBPATH +=  -L"libs/glm/gtc"

FRAMEWORKS = -framework OpenGL 
INCLUDES = -I/usr/local/include/SDL2 
INCLUDES += -Ilibs/glm/ 
INCLUDES += -Ilibs/glm/gtc
INCLUDES += -Ilibs/lua

CC = g++
COMPILER_FLAGS =  -W -DDEBUG
LINKER_FLAGS =  -lSDL2  -llua
BUILD_DIR = ../../build/
OBJ_NAME =  Game


#This is the target that compiles our executable

all : $(OBJS)
	mkdir -p $(BUILD_DIR)
	$(CC)  $(INCLUDES) $(FRAMEWORKS) $(LIBPATH)  $(LINKER_FLAGS) $(OBJS)  -o $(BUILD_DIR)$(OBJ_NAME)
	rm -rf $(BUILD_DIR)Assets
	cp -R Assets/ $(BUILD_DIR)Assets